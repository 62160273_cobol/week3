         IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. TEERARAK.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
      *HEIGHT
       01  H   PIC 999V99 . 
      *WEIGHT
       01  W   PIC 999V99.
      *BMI
       01  BMI      PIC 99V99.
      *TYPE
       01  BMI-TYPE PIC X(15).

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "Please input your height (cm) - " WITH NO ADVANCING 
           ACCEPT H 
           DISPLAY "Please input your weight (kg) - " WITH NO ADVANCING 
           ACCEPT W 
           COMPUTE BMI = W / (H/100)**2
           END-COMPUTE 
           DISPLAY "Your BMI is " BMI
           IF BMI < 18.5 THEN
              DISPLAY "UNDERWEIGHT"
           END-IF 
           IF BMI >= 18.5 AND BMI <= 24.9
              DISPLAY "NORMAL"
           END-IF 
           IF BMI >= 25.0 AND BMI <= 29.9
              DISPLAY "OVERWEIGHT"
           END-IF 
           IF BMI >= 30.0 AND BMI <= 34.9
              DISPLAY "OBESE"
           END-IF 
           IF BMI >=35.0
              DISPLAY "EXTREMLY OBESE"
           END-IF 
       .
